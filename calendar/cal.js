document.addEventListener('DOMContentLoaded', function() {
    const calendar = document.getElementById('calendar');
    const monthYear = document.getElementById('monthYear');
    const prevYearBtn = document.getElementById('prevYear');
    const nextYearBtn = document.getElementById('nextYear');
    const prevMonthBtn = document.getElementById('prevMonth');
    const nextMonthBtn = document.getElementById('nextMonth');

    let currentMonth = new Date().getMonth();
    let currentYear = new Date().getFullYear();

    const monthNames = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    function renderCalendar(month, year) {
        calendar.innerHTML = '';
        const daysInMonth = new Date(year, month + 1, 0).getDate();
        const monthStartDay = new Date(year, month, 1).getDay();

        monthYear.textContent = `${monthNames[month]} ${year}`;

        for (let i = 0; i < monthStartDay; i++) {
            const blank = document.createElement('div');
            blank.classList.add('day', 'blank');
            calendar.appendChild(blank);
        }

        for (let i = 1; i <= daysInMonth; i++) {
            const day = document.createElement('div');
            day.classList.add('day');
            day.textContent = i;
            day.addEventListener('click', function() {
                this.classList.toggle('selected');
            });
            calendar.appendChild(day);
        }
    }

    prevYearBtn.addEventListener('click', function() {
        currentYear--;
        renderCalendar(currentMonth, currentYear);
    });

    nextYearBtn.addEventListener('click', function() {
        currentYear++;
        renderCalendar(currentMonth, currentYear);
    });

    prevMonthBtn.addEventListener('click', function() {
        currentMonth--;
        if (currentMonth < 0) {
            currentMonth = 11;
            currentYear--;
        }
        renderCalendar(currentMonth, currentYear);
    });

    nextMonthBtn.addEventListener('click', function() {
        currentMonth++;
        if (currentMonth > 11) {
            currentMonth = 0;
            currentYear++;
        }
        renderCalendar(currentMonth, currentYear);
    });

    renderCalendar(currentMonth, currentYear);
});

